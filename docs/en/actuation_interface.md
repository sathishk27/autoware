# Actuation Interface 
Contains the necessary data for communication between Actuation Controller and Autoware Host PC.

## Vehicle Command
SteerCmd steer_cmd
 - int32 steer

AccelCmd accel_cmd
 - int32 accel

BrakeCmd brake_cmd
 - int32 brake

LampCmd lamp_cmd
 - int32 l
 - int32 r

int32 gear

int32 mode

TwistStamped twist_cmd              (# Linear and angular Velocity. Depends on where feedback is closed)

ControlCommand ctrl_cmd
 - float64 linear_velocity          (# m/s)
 - float64 linear_acceleration      (# m/s^2)
 - float64 steering_angle

int32 emergency

## Vehicle Status

#### Powertrain
int32 drivemode                     (# Auto, manual, Steering Test, Brake Test)

int32 steeringmode

 - MODE_MANUAL=0
 - MODE_AUTO=1

int32 gearshift  
 - DRIVE = 16
 - NEUTRAL = 32
 - REVERSE = 64
 - PARKING = 128

float64 speed                   (# vehicle velocity #m/s)

int32 drivepedal

int32 brakepedal

float64 angle                   (# vehicle steering (tire) angle #rad)

#### Body
int32 lamp
- LAMP_LEFT=1
- LAMP_RIGHT=2
- LAMP_HAZARD=3

int32 light
